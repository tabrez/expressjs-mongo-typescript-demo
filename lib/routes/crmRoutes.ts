import * as express from 'express';
import { Request, Response } from 'express';
import { ContactController } from '../controllers/crmController';

export class Routes {
    public contactController: ContactController;

    public constructor(contact: any) {
        this.contactController = new ContactController(contact);
    }

    public routes(app: express.Application): void {
        app.route('/')
            .get((_: Request, res: Response) =>
                res.status(200).send({
                    routes: 'GET /contact; POST /contact; ' +
                        'GET /contact/:id; PUT /contact/:id; DELETE /contact/:id'
                }));

        app.route('/contact')
            .get(this.contactController.getContacts)
            .post(this.contactController.addNewContact);

        app.route('/contact/:contactId')
            .get(this.contactController.getContactWithID)
            .put(this.contactController.updateContactWithID)
            .delete(this.contactController.deleteContactWithID);
    }
}
