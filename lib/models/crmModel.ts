import * as mongoose from 'mongoose';

export interface IContact extends mongoose.Document {
    firstName: String;
    lastName: String;
    email?: String;
    company?: String;
    phone?: String;
}

export const ContactSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: 'Enter a first name'
    },
    lastName: {
        type: String,
        required: 'Enter a last name'
    },
    email: {
        type: String
    },
    company: {
        type: String
    },
    phone: {
        type: Number
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});
