FROM node:11.10-alpine

# COPY . /app

COPY package.json /app/
COPY tsconfig.json /app/
COPY lib /app/lib
COPY tests /app/tests

# RUN npm install -g typescript
# RUN npm install -g ts-node
WORKDIR /app
RUN npm i

CMD ["npm", "run", "dev"]
