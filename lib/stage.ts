import app from './app';

app.listen(3000, () => {
    console.log('Connected to MongoDB server...');
    console.log('Express server started on port: 3000...');
    console.log('Closing connection...');
    process.exit(0);
});
