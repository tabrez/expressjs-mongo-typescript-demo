import { Request, Response } from 'express';
import { Utils } from '../utils';
import { boundMethod } from 'autobind-decorator';
import { IContact } from 'models/crmModel';

// OK to use @boundclass because all handlers need to be bound to `this`?
export class ContactController {

    private Contact: any;
    public constructor(contact: any) {
        this.Contact = contact;
    }

    @boundMethod
    public getContacts(_: Request, res: Response) {
        this.Contact.find({}, (err: any, contact: IContact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    @boundMethod
    public addNewContact(req: Request, res: Response) {
        const newContact = new this.Contact(req.body);

        const fn = newContact.firstName;
        if (fn.indexOf(' ') >= 0) {
            const names = Utils.splitFullName(fn);
            newContact.firstName = names[0];
            newContact.lastName = names[1];
        }

        newContact.save((err: any, contact: IContact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    @boundMethod
    public getContactWithID(req: Request, res: Response) {
        this.Contact.findById(req.params.contactId, (err: any, contact: IContact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    @boundMethod
    public updateContactWithID(req: Request, res: Response) {
        this.Contact.findByIdAndUpdate(req.params.contactId,
            req.body,
            (err: any, contact: IContact) => {
                if (err) {
                    res.send(err);
                }
                res.json(contact);
            });
    }

    @boundMethod
    public deleteContactWithID(req: Request, res: Response) {
        this.Contact.findByIdAndDelete(req.params.contactId, (err: any, contact: IContact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }
}
