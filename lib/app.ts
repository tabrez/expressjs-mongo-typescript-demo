import * as express from 'express';
import * as bodyParser from 'body-parser';
import { Routes } from './routes/crmRoutes';
import { IContact, ContactSchema } from './models/crmModel';
import * as mongoose from 'mongoose';

class App {
    public app: express.Application;
    public routePrv: Routes;
    private uri = process.env.MONGO_URI || 'mongodb://localhost:27017/CRMdb';

    public constructor() {
        this.app = express();
        this.config();
        const contact = mongoose.model<IContact>('Contact', ContactSchema);
        this.routePrv = new Routes(contact);
        this.routePrv.routes(this.app);
        this.mongoSetup();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }

    public mongoSetup(): void {
        mongoose.connect(this.uri,
            {
                useNewUrlParser: true
            }, (err) => {
                if (err)
                    console.log('Connection to mongodb failed: ' + err);
            }
        );
    }
}

export default new App().app;
