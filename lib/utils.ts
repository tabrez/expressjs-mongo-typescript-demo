import * as assert from 'assert';

export class Utils {
    public static splitFullName(fullName: String) {
        // assert (typeof fullName === 'string');
        assert(fullName !== '');
        const names = fullName.split(' ');
        // handle these kind of errors via validation at UI layer
        assert(names.length < 4);
        if (names.length == 1) {
            return [names[0]];
        }
        return [names[0], names[names.length - 1]];
    }
}
