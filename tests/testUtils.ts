import { Utils } from '../lib/utils';
import * as test from 'tape';

test('splitFullName: name with zero words should throw an assertion error',
    (t) => {
        t.throws(() => Utils.splitFullName(''));
        t.end();
    });

test('splitFullName: name with one word should be split into one part',
    (t) => {
        const actual = Utils.splitFullName('abc').length;
        const expected = 1;
        t.equal(actual, expected);
        t.end();
    });

test('splitFullName: name with two or three words should be split into two parts',
    (t) => {
        let actual = Utils.splitFullName('abc def').length;
        let expected = 2;
        t.equal(actual, expected);

        actual = Utils.splitFullName('abc def ghi').length;
        expected = 2;
        t.equal(actual, expected);
        t.end();
    });

test('splitFullName: name with four or more words should throw an assertion error',
    (t) => {
        t.throws(() => Utils.splitFullName('abc def ghi jkl'));
        t.end();
    });
